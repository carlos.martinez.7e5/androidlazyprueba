package com.example.affirmations.viewmodel

import com.example.affirmations.data.Datasource
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class MainViewModelTest{

    var mvmodel = MainViewModel()
    var d = Datasource()

    @Test
    fun listStartsEmpty(){
        assert(mvmodel.listOfBeasts.value == null)
    }

    @Test
    fun listIsNotNull(){
        mvmodel.getAllAffirmations()
        assert(mvmodel.listOfBeasts.value != null)
    }

    @Test
    fun listHasAllTheItems(){
        mvmodel.getAllAffirmations()
        assert(mvmodel.listOfBeasts.value!!.count() == d.loadAllBeasts().count())
    }
}