package com.example.affirmations.viewmodel

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Beast
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class DetailViewModel : ViewModel() {

    /*State son los datos con los que ViewModel trabaja, modifica, muestra.
    * Pueden ser una clase aparte o no, en este caso es la misma clase Beast*/

    //Los datos de State que sólo este ViewModel puede modificar, y que utilizará para trabajar
    private val _detailedBeast = MutableStateFlow<Beast?>(null)

    /*Copia pública que la respectiva activity puede consultar, pero solo "de lectura"
    * AsStateFlow es como una declaración de evento, cuado se actualice, enviará "una señal"
    * y los que estén "suscritos" la recibirán y actualizaran el valor*/
    val detailedBeast : StateFlow<Beast?> = _detailedBeast.asStateFlow()

    fun getDetailedBeast(param_id: Int) {

        val dS = Datasource()

        dS.loadAllBeasts().forEachIndexed { _, affirmation ->
            if (affirmation.id == param_id) _detailedBeast.value = affirmation
        }

        // _uiState.value = dS.loadAffirmations().firstOrNull { it.id == param_id }
    }

    fun sendMail(context: Context, to: String, subject: String){
        try {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "message/rfc822"
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
            intent.putExtra(Intent.EXTRA_SUBJECT, subject)
            context.startActivity(intent)
        } catch (t: Throwable){
            println("No funcionó") //<- Para que no se quede vacío y de warning
        }
    }

    fun callPhone(context: Context, phoneNum: String){
        try {
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNum, null))
            context.startActivity(intent)
        } catch (t: Throwable){
            println("No funcionó") //<- Para que no se quede vacío y de warning
        }
    }
}
