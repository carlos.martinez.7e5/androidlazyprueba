package com.example.affirmations.viewmodel

import com.example.affirmations.data.Datasource
import dalvik.annotation.TestTarget
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class DetailViewModelTest{

    var dvmodel = DetailViewModel()
    var d = Datasource()

    @Test
    fun specificBeastStartsBeeingNull(){
        assert(dvmodel.detailedBeast.value == null)
    }

    @Test
    fun specificBeastIsNotNull(){
        dvmodel.getDetailedBeast(3567)
        assert(dvmodel.detailedBeast != null)
    }

    @Test
    fun specificBeastIsTheCorrectOne() {
        dvmodel.getDetailedBeast(3567) //Es la primera de datasource
        assert(dvmodel.detailedBeast.value == d.loadAllBeasts().first())
    }
    @Test
    fun specificBeastWithIncorrectIdDoNotExist(){
        dvmodel.getDetailedBeast(11037)
        assert(dvmodel.detailedBeast.value == null)
    }
}