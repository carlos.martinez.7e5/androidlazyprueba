package com.example.affirmations.viewmodel

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.example.affirmations.DetailActivity
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Beast
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class MainViewModel : ViewModel() {

    //Los datos de State que sólo este ViewModel puede modificar, y que utilizará para trabajar
    private val _listOfBeasts = MutableStateFlow<List<Beast>?>(null)

    /*Copia pública que la respectiva activity puede consultar, pero solo "de lectura"
    * AsStateFlow es como una declaración de evento, cuado se actualice, enviará "una señal"
    * y los que estén "suscritos" la recibirán y actualizaran el valor*/
    val listOfBeasts: StateFlow<List<Beast>?> = _listOfBeasts.asStateFlow()

    /* Función para obtener una lista con todas las affirmations de Datasource.
    * Realmente hace lo mismo, en vez de devolver una lista actualiza el estado, para que la activity
    * lo pueda usar de manera más "limpia" y se separe la lógica y la parte estética */
    fun getAllAffirmations() {
        _listOfBeasts.value = Datasource().loadAllBeasts()
    }

    fun intent(context: Context, param_id: Int) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("id", param_id)
        context.startActivity(intent)
    }
}