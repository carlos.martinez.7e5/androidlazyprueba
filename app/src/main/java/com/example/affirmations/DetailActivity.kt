package com.example.affirmations

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.viewmodel.DetailViewModel
class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContent {
            Detail()
        }
    }

    @Composable
    fun Detail() {
        val context = LocalContext.current
        val dViewModel: DetailViewModel = viewModel()
        val detailedBeast by dViewModel.detailedBeast.collectAsState()

        //El cero es el default, asumo que el que pone si no encuentra nada
        val id: Int = intent.getIntExtra("id", 0)

        dViewModel.getDetailedBeast(param_id = id)

        detailedBeast?.let { beast ->
            Column(
                modifier = Modifier
                    .fillMaxSize()
            ) {

                //La imagen en grande
                Image(
                    painter = painterResource(id = beast.imageID),
                    contentDescription = "La imagen",
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(300.dp)
                        .padding(top = 20.dp, start = 20.dp, end = 20.dp)
                )

                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(start = 20.dp, end = 20.dp)
                ) {
                    //El título
                    Text(
                        text = stringResource(id = beast.nameId),
                        fontSize = 30.sp,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = 10.dp),
                        textAlign = TextAlign.Center
                    )

                    //La descripción larga
                    Text(
                        text = stringResource(id = beast.descriptionID),
                        fontSize = 20.sp
                    )

                    Row(
                        horizontalArrangement = Arrangement.End,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 30.dp)
                    ) {

                        //Subject "personalizado" a cada bestia
                        val subject = "Dear " + stringResource(id = beast.nameId)

                        IconButton(onClick = {
                            dViewModel.sendMail(
                                context,
                                beast.email,
                                subject
                            )
                        }, modifier = Modifier
                            .padding(end = 25.dp)
                            .size(45.dp)) {
                            Icon(
                                painter = painterResource(id = R.drawable.mail_icon),
                                contentDescription = "Icono de un e-mail"
                            )
                        }

                        IconButton(
                            onClick = { dViewModel.callPhone(context, beast.phone) },
                            modifier = Modifier.size(45.dp)
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.phone_icon),
                                contentDescription = "Icono de un telefono"
                            )
                        }
                    }
                }
            }
        }
    }
}