/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations.data
import com.example.affirmations.R
import com.example.affirmations.model.Beast

class Datasource() {
    fun loadAllBeasts(): List<Beast> {
        return listOf<Beast>(
            Beast(R.string.affirmation1, R.drawable.image1,R.string.description1, 3567,"5553499786","qvmfxvdr@gmail.com"),
            Beast(R.string.affirmation2, R.drawable.image2,R.string.description2,9298,"6842918331","rwpdsjfi@gmail.com"),
            Beast(R.string.affirmation3, R.drawable.image3,R.string.description3, 4132,"8905774496","byfndate@gmail.com"),
            Beast(R.string.affirmation4, R.drawable.image4,R.string.description4,6701,"3354389087","xmfodgjv@gmail.com"),
            Beast(R.string.affirmation5, R.drawable.image5,R.string.description5,8723,"7122385624","nhqxlbrd@gmail.com"),
            Beast(R.string.affirmation6, R.drawable.image6,R.string.description6,1379,"4827991799","dtvznbpm@gmail.com"),
            Beast(R.string.affirmation7, R.drawable.image7,R.string.description7,5489,"1867850243","kxwemcjs@gmail.com"),
            Beast(R.string.affirmation8, R.drawable.image8,R.string.description8,2156,"9294166037","rhwxpgai@gmail.com"),
            Beast(R.string.affirmation9, R.drawable.image9,R.string.description9,7643,"2735672978","oyhjizsp@gmail.com"),
            Beast(R.string.affirmation10, R.drawable.image10,R.string.description10,3085,"5369937011","vaxlkfge@gmail.com"))
    }
}
