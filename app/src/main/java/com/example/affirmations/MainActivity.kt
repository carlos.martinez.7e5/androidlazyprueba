package com.example.affirmations

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Beast
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.viewmodel.MainViewModel
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContent {
            AffirmationApp()
        }
    }
}

@SuppressLint("StateFlowValueCalledInComposition")
@Composable
fun AffirmationApp() {
    AffirmationsTheme {
        Column {
            AffirmationList()
        }
    }
}

@Composable
fun AffirmationList(modifier: Modifier = Modifier) {

    val viewModel : MainViewModel = viewModel()
    viewModel.getAllAffirmations()

    //"Suscripción al evento" asStateFlow. De esta manera siempre estará actualizada
    val listOfBeasts by viewModel.listOfBeasts.collectAsState()

    //De esta manera no le hay que pasar parámetro, sino que recibe directamente del ViewModel
    //y lo utiliza.
    LazyColumn(modifier = Modifier.background(Color.LightGray)) {
        listOfBeasts?.let {
            items(it.count()){
                index ->
                AffirmationCard(beast = listOfBeasts!![index], viewModel = viewModel)
            }
        }
    }
}

@Composable
fun AffirmationCard(beast: Beast, modifier: Modifier = Modifier, viewModel: MainViewModel = viewModel()) {

    val isExpanded = remember { mutableStateOf(false) }
    val context = LocalContext.current

    Card(modifier = Modifier.padding(6.dp), border = BorderStroke(1.dp, color = Color.Blue)) {
        Column(
            modifier = Modifier.animateContentSize(
                animationSpec = spring(
                    dampingRatio = Spring.DampingRatioMediumBouncy,
                    stiffness = Spring.StiffnessLow
                )
            )
        ) {
            Row(verticalAlignment = Alignment.CenterVertically, modifier = Modifier.fillMaxSize()) {
                Image(
                    modifier = Modifier
                        .fillMaxHeight()
                        .padding(10.dp),
                    painter = painterResource(id = beast.imageID),
                    contentDescription = "Imagen de la carta, muestra un paisaje"
                )
                Text(
                    fontSize = 18.sp,
                    text = stringResource(id = beast.nameId),
                    modifier = Modifier
                        .weight(1f)
                        .padding(end = 10.dp)
                        .padding(vertical = 10.dp)
                )
                ExpandIcon(
                    expanded = isExpanded.value,
                    click = {
                        isExpanded.value = !isExpanded.value
                        println(isExpanded.value)
                    },
                    modifier = Modifier.padding(start = 10.dp)
                )
            }

            if (isExpanded.value) {

                Column(Modifier.fillMaxWidth().padding(start = 10.dp, end = 10.dp)) {
                    AffirmationCardDetails(detail = beast.descriptionID, modifier = Modifier)
                    TextButton(
                        onClick = { viewModel.intent(context, beast.id) },
                        modifier = Modifier.align(Alignment.End)
                    ) {
                        Text(text = "See more")
                    }
                }
            }
        }
    }
}

@Composable
fun AffirmationCardDetails(detail: Int, modifier: Modifier) {
    Text(text = stringResource(id = detail), fontSize = 22.sp, overflow = TextOverflow.Ellipsis, maxLines = 2)
}

//Aquí se crea la clase "botón icono".
//Más adelante se llama para usarla, pero aqui solo se "hace", se prepara para usar cuando la llamen
@Composable
private fun ExpandIcon(
    expanded: Boolean, //Para saber si esta expandida o no
    click: () -> Unit, //Para saber qué tiene que hacer al hacer click
    modifier: Modifier = Modifier //El modifier
) {
    //El click es el parámetro que se le pasa a la función
    IconButton(onClick = click) {
        Icon(
            imageVector =
            if (expanded) Icons.Filled.ExpandLess
            else Icons.Filled.ExpandMore,
            tint = MaterialTheme.colors.secondary,
            contentDescription = "Icono que muestra una flecha hacia abajo"
        )
    }
}
@Preview
@Composable
private fun AffirmationCardPreview() {
    val d = Datasource()
    AffirmationCard(beast = d.loadAllBeasts()[0])
}
